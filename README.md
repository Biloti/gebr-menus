![109334132-gebr-logo-small.png](https://bitbucket.org/repo/5bLeRq/images/3321240599-109334132-gebr-logo-small.png)

**GêBR** is a free-software which provides an environment to seismic data processing, designed to easily assemble and run processing flows, as well as to manage jobs.

Menus is the entity that brings functionality to GêBR. This project implements several menus to GêBR, organized in categories:

* Seismic Unix - an open-source seismic-processing package developed at Center for Wave Phenomena.
* Shell tools - many useful shell tools

More about of GêBR is available at [GêBR Home Page](http://www.gebrproject.com/).